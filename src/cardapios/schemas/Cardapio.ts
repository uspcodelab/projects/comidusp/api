import * as mongoose from 'mongoose';

export const CardapioSchema: mongoose.Schema = new mongoose.Schema({
    restaurante: String,
    prato: Object,
    data: String,
    refeicao: String,
});
