import { Controller, Get, Param, Body, Post, Delete, Put } from '@nestjs/common';
import { CriaCardapioDTO } from './DTOs/CriaCardapioDTO';

import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { HttpStatus, HttpException } from '@nestjs/common';
import { AtualizaCardapioDTO } from './DTOs/AtualizaCardapioDTO';
import { CardapiosService } from './cardapios.service';

@Controller('restaurantes/:nomeRestaurante/cardapios/')
export class CardapiosController {
  constructor(private readonly cardapiosService: CardapiosService) {}

  @Get()
  async findAll(@Param() params): Promise<any[]> {
    return this.cardapiosService.findAll(params.nomeRestaurante);
  }

  @Get(':data/:refeicao')
  async findOne(@Param() params): Promise<any> {
    const {restaurante, data, refeicao} = params;
    return this.cardapiosService.findOne(restaurante, data, refeicao);
  }

  @Post()
  async create(@Body() dto: CriaCardapioDTO): Promise<any> {
    if (! this.cardapiosService.validateCardapio(dto)) {
      throw new HttpException('Invalid body', HttpStatus.BAD_REQUEST);
    }

    return await this.cardapiosService.create(dto);
  }

  @Delete(':data/:refeicao')
  async delete(@Param() params): Promise<any> {
    return await this.cardapiosService.delete(params.data, params.refeicao);
  }

  @Put(':data/:refeicao')
  async update(@Param() params, @Body() atualizaCardapioDTO: AtualizaCardapioDTO): Promise<any> {
    return await this.cardapioModel.findOneAndUpdate({
      data: params.data,
      refeicao: params.refeicao,
    }, atualizaCardapioDTO);
  }
}
