import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class CardapiosService {
  constructor(@InjectModel('Cardapios') private readonly cardapiosModel: Model<any>) { }

  async findAll(nomeRestaurante: string): Promise<any[]> {
    return await this.cardapiosModel.find({ restaurante: nomeRestaurante });
  }

  async findOne(restaurante: string, data: string, refeicao: string): Promise<any> {
    return await this.cardapiosModel.findOne({
      restaurante,
      data,
      refeicao,
    });
  }
  
  async create(dto): Promise<any> {
    const novoCardapio: any = new this.cardapiosModel(dto);
    return await novoCardapio.save();
  }

  async delete(data: any, refeicao: string): Promise<any> {
    return await this.cardapiosModel.findOneAndDelete({
      data,
      refeicao,
    });
  }

  public validateCardapio(cardapio: any): boolean {
    if (!cardapio.restaurante || (cardapio.restaurante as string).length < 3 || (cardapio.restaurante as string).length > 50) {
      return false;
    }

    if (!cardapio.prato || (cardapio.prato as string).length < 3 || (cardapio.prato as string).length > 50) {
      return false;
    }

    if (!cardapio.data || (cardapio.data as string).length < 6) {
      return false
    }

    if (!cardapio.refeicao || Number.parseInt(cardapio.refeicao) > 2 || Number.parseInt(cardapio.refeicao) < 0) {
      return false;
    }

    return true;
  }

}
