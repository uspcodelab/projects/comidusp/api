import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CardapiosController } from './cardapios.controller';
import { CardapioSchema } from './schemas/Cardapio';
import { CardapiosService } from './cardapios.service';

@Module({
  controllers: [CardapiosController],
  imports: [MongooseModule.forFeature([{ name: 'Cardapio', schema: CardapioSchema }])],
  providers: [CardapiosService],
})
export class CardapiosModule {}
