export class AtualizaCardapioDTO {
    readonly prato: object;
    readonly data: string;
    readonly refeicao: string;
}