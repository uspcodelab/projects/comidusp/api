export class CriaCardapioDTO {
    readonly restaurante: string;
    readonly prato: object;
    readonly data: string;
    readonly refeicao: string;
}