import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RestaurantesModule } from './restaurantes/restaurantes.module';
import { CardapiosModule } from './cardapios/cardapios.module';
import { CardapioService } from './cardapio/cardapio.service';

@Module({
  imports: [
    RestaurantesModule,
    MongooseModule.forRoot('mongodb://mongo:27017/nest'),
    CardapiosModule,
  ],
  providers: [CardapioService],
})
export class AppModule {}
