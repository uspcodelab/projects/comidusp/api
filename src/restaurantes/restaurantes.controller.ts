import { Controller, Get, Post, Put, Body, HttpStatus, HttpException, Delete, Param } from '@nestjs/common';
import { CriaRestauranteDTO } from './DTOs/CriaRestauranteDTO';
import { RestaurantesService } from './restaurantes.service';
import { AtualizaRestauranteDTO } from './DTOs/AtualizaRestauranteDTO';

@Controller('restaurantes')
export class RestaurantesController {
  constructor(private readonly restaurantesService: RestaurantesService) { }

  @Get()
  async findAll(): Promise<any[]> {
    return await this.restaurantesService.findAll();
  }

  @Get(':nome')
  async findOne(@Param() params): Promise<any> {
    return await this.restaurantesService.findOne(params.nome);
  }

  @Post()
  async create(@Body() dto: CriaRestauranteDTO): Promise<any> {
    // const novoRestaurante: any = {...dto};
    if (!this.validateRestaurante(dto)) {
      throw new HttpException('Invalid body', HttpStatus.BAD_REQUEST);
    }
    return await this.restaurantesService.create(dto);
  }

  private validateRestaurante(restaurante: any): boolean {

    if (!restaurante.rua || (restaurante.rua as string).length < 3 || (restaurante.rua as string).length > 50) {
      return false;
    }

    if (!restaurante.cep || restaurante.cep.length !== 8) {
      return false;
    }

    if (!restaurante.numero || restaurante.numero.length === 0) {
      return false;
    }

    return true;
  }

  @Delete(':nome')
  async delete(@Param('nome') nome: string): Promise<any> {
    return await this.restaurantesService.delete(nome);
  }

  @Put(':nome')
  async update(
    @Param('nome') nome: string,
    @Body() atualizaRestauranteDto: AtualizaRestauranteDTO,
  ): Promise<any> {
    return await this.restaurantesService.update(nome, atualizaRestauranteDto);
  }
}
