import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RestaurantesController } from './restaurantes.controller';
import { RestaurantesService } from './restaurantes.service';
import { RestauranteSchema } from './schemas/Restaurante';

@Module({
  controllers: [RestaurantesController],
  providers: [RestaurantesService],
  imports: [MongooseModule.forFeature([{ name: 'Restaurante', schema: RestauranteSchema }])],
})
export class RestaurantesModule {}
