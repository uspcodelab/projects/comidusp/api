export class AtualizaRestauranteDTO {
    readonly nome: string;
    readonly rua: string;
    readonly cep: string;
    readonly numero: string;
}
