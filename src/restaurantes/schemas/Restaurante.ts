import * as mongoose from 'mongoose';

export const RestauranteSchema: mongoose.Schema = new mongoose.Schema({
  nome: {
    type: String,
    unique: true,
  },
  rua: String,
  cep: String,
  numero: String,
});
