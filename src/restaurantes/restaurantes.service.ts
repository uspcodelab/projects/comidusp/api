import { Model } from 'mongoose';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { CriaRestauranteDTO } from './DTOs/CriaRestauranteDTO';
import { InjectModel } from '@nestjs/mongoose';
import { AtualizaRestauranteDTO } from './DTOs/AtualizaRestauranteDTO';

@Injectable()
export class RestaurantesService {

  constructor(@InjectModel('Restaurante') private readonly restauranteModel: Model<any>) { }

  async findAll(): Promise<any[]> {
    return await this.restauranteModel.find();
  }

  async findOne(nome: string): Promise<any> {
    return await this.restauranteModel.findOne({ nome });
  }

  async create(dto: CriaRestauranteDTO): Promise<any> {
    const novoRestaurante: any = new this.restauranteModel(dto);
    return await novoRestaurante.save();
  }

  async delete(nome: string): Promise<any> {
    return await this.restauranteModel.findOneAndDelete({ nome });
  }

  async update(nome: string, atualizaRestauranteDto: AtualizaRestauranteDTO): Promise<any> {
    return await this.restauranteModel.findOneAndUpdate({ nome }, atualizaRestauranteDto);
  }
}
